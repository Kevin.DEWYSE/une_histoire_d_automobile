from functions.edit_csv.change_format_csv import change_format_csv
from functions.files_functions.f_exist import f_exist
# Chemin relatif des fichiers

csv_in = 'csv/auto.csv'
csv_out = 'csv/out.csv'

# Keys = numéro colonne
# Values = nom colonne

fieldnames = {
    0:'adresse_titulaire',
    10:'nom',
    7:'prenom',
    8:'immatriculation',
    18:'date_immatriculation',
    14:'vin',
    9:'marque',
    5:'denomination_commerciale',
    3:'couleur',
    1:'carrosserie',
    2:'categorie',
    4:'cylindree',
    6:'energie',
    11:'places',
    12:'poids',
    13:'puissance',
    15:'type',
    16:'variante',
    17:'version'
}
# Vérifier si auto.csv, out.csv existe sinon on les crée
f_exist(csv_in), f_exist(csv_out)

# Appel de la fonction qui va permettre de tout transformer au niveau du .csv
change_format_csv(csv_in, csv_out,fieldnames,';')
        