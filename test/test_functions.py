import unittest

from functions.edit_csv.change_format_date import change_format_date
from functions.edit_csv.cut_word import cut_word
from functions.edit_csv.order import order
from functions.edit_csv.groupe_data import groupe_data

class TestEditCsvFunctions(unittest.TestCase):
    
    def test_format_date(self):
        self.assertEqual(change_format_date( ["1974-04-01"],0),"01-04-1974")
        # No match format
        # self.assertNotEqual(change_format_date( ["01-04-1974"],0),"01-04-1974")
        # self.assertNotEqual(change_format_date( ["01-1974-04"],0),"01-04-1974")
        
    def test_cut_word(self):
        word = ["type_variante_version"]
        self.assertEqual(cut_word(word,0,"_"),["type","variante","version"])
    
    def test_order(self):
        
        dictionnary = {
            2:"type_1",
            1:"type_2",
            0:"type_3"
        }
        
        list_string = ["type_1","type_2","type_3"]
        
        self.assertEqual(order(list_string,dictionnary.keys()),["type_3","type_2","type_1"])
    def test_groupe_date(self):
        list_original= ["type_1","type_2"]
        list_string = ["type_2M"]
        date = "05-14-1423"
        self.assertEqual(groupe_data(list_original,[1],list_string,date),["type_1","type_2M","05-14-1423"])
        