from datetime import datetime

def change_format_date(row,index):
    """
    Change le format d'une date : annee mois jour a mois jour annee
    
    Parametre : liste, entier
    
    Retour : chaine de caractere
    """
    current_date= datetime.strptime(list(row)[index],'%Y-%m-%d')
    return current_date.strftime('%d-%m-%Y')