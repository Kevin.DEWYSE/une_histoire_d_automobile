def order(list_in,ord_keys):
    """
    Trie les données dans une liste selon un ordre donné 
    via un les clés d'un dictionnaire
    
    Parametre : liste, dictionnaire de valeurs
    
    Retour : Liste temporaire avec le contenu ordonné
    """
    tmp_list = []
    [tmp_list.append(list_in[index]) for index in ord_keys]
    return tmp_list