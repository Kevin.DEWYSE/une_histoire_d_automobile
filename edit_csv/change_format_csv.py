import csv
from functions.edit_csv.change_format_date import change_format_date
from functions.edit_csv.cut_word import cut_word
from functions.edit_csv.groupe_data import groupe_data
from functions.edit_csv.order import order

def change_format_csv(file_in,file_out,fieldnames,p_delimiter): 
    """
    Changement des delimiteurs, noms colonnes, ordre et 
    ecrit tous les changements dans un fichier externe
    
    Parametre : Chaine de caractere, chaine de caractere, dictionnaire, caractere
    """
    with open(file_in) as csv_in, open(file_out,'w+',newline='') as csv_out:
        # preparation de l'ecriture dans le fichier out.csv
        writer = csv.writer(csv_out,delimiter=p_delimiter)
        # Ecriture des noms de colonnes
        writer.writerow(fieldnames.values())
        # Parcours toutes les lignes du fichiers a partir de la ligne 1
        for row in list(csv.reader(csv_in,delimiter='|'))[1:]:
            # Modification de la ligne, couper mot (type_variante_version) et changer format date
            list_row = groupe_data(row,[15,5],cut_word(row,15,", "),change_format_date(row,5))
            # Triage et ecriture de la list dans le fichier out.csv
            writer.writerow(order(list_row,fieldnames.keys()))