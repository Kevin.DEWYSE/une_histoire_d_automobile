def cut_word(row,index,separator):
    """
    Coupe un mot selon un séparateur
    
    Parametre : liste, entier, chaine de caractere
    
    Retour : liste temporaire contenant les valeurs decoupees
    """
    tmp_list = []
    chaine = list(row)[index].split(separator)
    [tmp_list.append(tmp_data) for tmp_data in chaine]
    return tmp_list