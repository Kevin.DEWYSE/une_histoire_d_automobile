def groupe_data(row,index_list,tmp_list, date):
    """
    Ajout de date a la liste temporaire et supression d'element dans 
    la liste principale (row), regroupe les deux listes : principal et temporaire
    
    Parametre : liste,liste d'entier, liste temporaire (cut_word), 
    chaine de caractere (change_format_date)
    
    Retour : la liste principal
    """
    tmp_list.append(date)
    [row.pop(index) for index in index_list]
    [row.append(tmp_data) for tmp_data in tmp_list]
    return row