import os
from functions.files_functions.f_create import f_create

def f_exist(file):
    """
    Verifie l'existance d'un fichier sinon il va le cree via la fonction (f_create)
    
    Parametre : chaine de caractere
    """
    if not isinstance(file,str):
        print("error:type_file_directory:isNotStr")
    if not os.path.exists(file):
        f_create(file)
        