import os
import sys

def f_remove(file):
    """
    Supprime un fichier, une exception est leve si le chemin
    n est pas bien specifie ou n existe pas
    
    Parametre : chaine de caractere
    """
    try:
        # Supprime un fichier
        os.remove(file)
    except FileNotFoundError as e:
        # Affiche l'erreur
        print("error:f_remove:" + str(e))
        sys.exit()