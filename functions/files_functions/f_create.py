import sys

def f_create(file):
    """
    Essaye d'ouvrir un fichier sinon il le cree
    si le repertoire n'est pas trouve il affiche une exception
    
    Parametre : chaine de caractere
    """
    try:
        # Ouvre un fichier en lecture ecriture
        with open(file,'w+') as target:
            print("creation:" + str(file))
    except FileNotFoundError as e:
        # Affiche l'erreur
        print("error:f_create:" + str(e))
        sys.exit()     